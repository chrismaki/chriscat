#developed by Krisztian Markella
#!/bin/bash

# how to use:
# ./chriscat.sh constants.h function_prototypes.h function_definitions.cpp main.cpp
# for multiple files or
# ./chriscat.sh your_assignment.cpp
# for only one cpp file.
# be sure that the script executable
# chmod 744 chriscat will give you the permission to run this script

SEPARATOR="#####"
for arg in $@; 
do
	printf "%s begining of the file : %s %s\n" "$SEPARATOR" "$arg" "$SEPARATOR"
	cat -n $arg
	printf "%s end of the file : %s %s\n\n" "$SEPARATOR" "$arg" "$SEPARATOR"
done